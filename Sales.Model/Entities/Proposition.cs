﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class Proposition : IIdentityObject
    {
        public int Id { get; set; }

        public decimal ProposalPrice { get; set; }

        public Product ExchangeProduct { get; set; }

        public bool? Accepted { get; set; }

        public User ProposerUser { get; set; } 
        
        public DateTime ReleaseDate { get; set; }
    }
}
