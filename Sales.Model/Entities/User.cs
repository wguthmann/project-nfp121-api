﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class User : IIdentityObject
    {
        public int Id { get; set; }
       
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public City City { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
