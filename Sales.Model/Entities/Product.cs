﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class Product : IIdentityObject
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set;  }

        public City City { get; set; }

        public User OwnerUser { get; set; }
        public ProductType ProductType { get; set; }
    }
}
