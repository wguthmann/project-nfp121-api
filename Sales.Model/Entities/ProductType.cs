﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class ProductType : IIdentityObject
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
