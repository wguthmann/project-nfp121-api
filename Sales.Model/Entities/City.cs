﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class City : IIdentityObject
    {
        public int Id { get; set; }

        public string CityName { get; set; }
        public string Country { get; set; }

        public DateTime ReleaseDate { get; set; }
    }

    
}
