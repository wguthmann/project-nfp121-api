﻿using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class Message : IIdentityObject
    {
        public int Id { get; set; }

        public String Text { get; set; }

        public String LinkPicture { set; get; }

        public DateTime ReleaseDate { get; set; }
    }
}
