﻿using System;
using System.ComponentModel.DataAnnotations;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class Sale : IIdentityObject
    {
        public int Id { get; set; }

        [Required]
        public float Price { get; set; }
        [Required]
        public User Buyer { get; set; }
        [Required]
        public User Seller { get; set; }
        [Required]
        public Product SaleProduct { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
