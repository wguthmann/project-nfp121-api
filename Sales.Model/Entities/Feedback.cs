﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales.Model.Interface;

namespace Sales.Model.Entities
{
    public class Feedback : IIdentityObject
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public float Rate { get; set; }

        public User RaterUser { get; set; }

        public User TargetUser { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}
