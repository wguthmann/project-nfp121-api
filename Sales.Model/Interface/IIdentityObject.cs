﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Model.Interface
{
    public interface IIdentityObject
    {
        public int Id { get; set; }
    }
}
