﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sales.Model.Tests.Entities
{
    [TestClass]
    public class UserTester
    {
        [TestMethod]
        public void CreateUsers_WithCorrectFields_ThenOk()
        {

        }

        [TestMethod]
        public void CreateUser_WithWrongField_ThenError()
        {

        }

        [TestMethod]
        public void GetAllUsers_With2UsersInDb_ThenList2()
        {

        }

        [TestMethod]
        public void GetOneUser_WithId2_ThenGetUser2()
        {

        }

        [TestMethod]
        public void PatchOneUser_WithCorrectField_ThenOk()
        {

        }
    }
}
