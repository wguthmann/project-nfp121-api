# project-nfp121-api

API : Sale of second-hand games

The idea is quite simple, to allow the connection of person wishing to resell their old games or carry out an exchange.
The application will be realized in the form of Android mobile application.

## Features offered by the API
-  Register and connect the user to the platform 
-  My account
-  Consultation of games made available by other users 
-  Request for Exchange
-  Scoring of games 
-  Ratings of resellers 
-  “You’ll like it too”

## User registration and login to the platform



## My account
Each user can configure in his account: 
-  A telephone number 
-  An email address 
This information will be used to get in touch via the application.



# Development
## Dotnet migrations

New migration :

```dotnet ef migrations add [migration_name] --project Sales.Model --startup-project SecondLife```

Update :

```dotnet ef database update [migration_name] --project Sales.Model --startup-project SecondLife```