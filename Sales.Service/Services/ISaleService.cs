﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;

namespace Sales.Services.Services
{
    public interface ISaleService : IService<Sale>
    {
    }

    public interface IUserService : IService<User>
    {
    }

    public interface IProductService : IService<Product>
    {
    }

    public interface IProductTypeService : IService<ProductType>
    {
    }

    public interface IPropositionService : IService<Proposition>
    {
    }

    public interface IFeedbackService : IService<Feedback>
    {
    }

    public interface ICityService : IService<City>
    {
    }

    public interface IService<T> where T : class
    {
        public List<T> List();
        T Get(in int id);
        T Add(T sale);
        T Patch(in int id, JsonPatchDocument<T> jsonPatch);
        T Remove(T sale);
    }
}
