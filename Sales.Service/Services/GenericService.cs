﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;

namespace Sales.Services.Services
{
    public class GenericService<T> : IService<T> where T : class
    {
        protected IRepository<T> _repo;
        protected IValidator<T> _validator;

        public GenericService(IRepository<T> repo, IValidator<T> validator)
        {
            _repo = repo;
            _validator = validator;
        }

        public List<T> List()
        {
            return _repo.All();
        }

        public T Get(in int id)
        {
            return _repo.One(id);
        }

        public virtual T Add(T sale)
        {
            if (_validator.CanAdd(sale))
            {
                return _repo.Add(sale);
            }

            return null;
        }

        public T Patch(in int id, JsonPatchDocument<T> jsonPatch)
        {
            var res = Get(id);
            if (res == null)
            {
                return null;
            }
            jsonPatch.ApplyTo(res);//result gets the values from the patch request
            return res;
        }

        public T Remove(T obj)
        {
            if (obj == null)
            {
                return null;
            }
            _repo.Remove(obj);
            return obj;
        }
    }
}