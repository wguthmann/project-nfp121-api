﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class ProductService : IProductService
    {
        private IProductRepository _repo;

        public ProductService(IProductRepository repo)
        {
            _repo = repo;
        }

        public List<Product> List()
        {
            return _repo.All();
        }

        public Product Get(in int id)
        {
            return _repo.One(id);
        }

        public Product Add(Product product)
        {
            if (product.Title != null && product.OwnerUser != null)
            {
                return null;
            }

            if (_repo.Exists(product))
            {
                return null;
            }

            return _repo.Add(product);
        }

        public Product Patch(in int id, JsonPatchDocument<Product> jsonPatch)
        {
            var product = Get(id);
            if (product == null)
            {
                return null;
            }
            jsonPatch.ApplyTo(product);//result gets the values from the patch request
            return product;
        }

        public Product Remove(Product product)
        {
            if (product == null)
            {
                return null;
            }
            _repo.Remove(product);
            return product;
        }
    }
}
