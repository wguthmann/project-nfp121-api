﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class ProductTypeService : IProductTypeService
    {
        private IProductTypeRepository _repo;

        public ProductTypeService(IProductTypeRepository repo)
        {
            _repo = repo;
        }


        public List<ProductType> List()
        {
            return _repo.All();
        }

        public ProductType Get(in int id)
        {
            return _repo.One(id);
        }

        public ProductType Add(ProductType productType)
        {
            if (productType.Type != null)
            {
                return null;
            }

            if (_repo.Exists(productType))
            {
                return null;
            }

            return _repo.Add(productType);
        }

        public ProductType Patch(in int id, JsonPatchDocument<ProductType> jsonPatch)
        {
            var productType = Get(id);
            if (productType == null)
            {
                return null;
            }
            jsonPatch.ApplyTo(productType);//result gets the values from the patch request
            return productType;
        }

        public ProductType Remove(ProductType productType)
        {
            if (productType == null)
            {
                return null;
            }
            _repo.Remove(productType);
            return productType;
        }
    }
}
