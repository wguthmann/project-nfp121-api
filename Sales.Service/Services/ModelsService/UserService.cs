﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class UserService : IUserService
    {
        private IUserRepository _repo;

        public UserService(IUserRepository repo)
        {
            _repo = repo;
        }

        public List<User> List()
        {
            return _repo.All();
        }

        public User Get(in int id)
        {
            return _repo.One(id);
        }

        public User Add(User user)
        {
            if (user.Username != null && user.Password != null)
            {
                return null;
            }

            if (_repo.Exists(user))
            {
                return null;
            }

            return _repo.Add(user);
        }

        public User Patch(in int id, JsonPatchDocument<User> jsonPatch)
        {
            var user = Get(id);
            if (user == null)
            {
                return null;
            }
            jsonPatch.ApplyTo(user);//result gets the values from the patch request
            return user;
        }

        public User Remove(User user)
        {
            if (user == null)
            {
                return null;
            }
            _repo.Remove(user);
            return user;
        }
    }
}
