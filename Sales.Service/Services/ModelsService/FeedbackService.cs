﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Services.Services.ModelsService
{
    public class FeedbackService : IFeedbackService
    {
        private IFeedbackRepository _repo;

        public FeedbackService(IFeedbackRepository repo)
        {
            _repo = repo;
        }

        public List<Feedback> List()
        {
            return _repo.All();
        }

        public Feedback Get(in int id)
        {
            return _repo.One(id);
        }

        public Feedback Add(Feedback feedback)
        {
            if (feedback.RaterUser != null && feedback.TargetUser != null)
            {
                return null;
            }

            if (_repo.Exists(feedback))
            {
                return null;
            }

            return _repo.Add(feedback);
        }

        public Feedback Patch(in int id, JsonPatchDocument<Feedback> jsonPatch)
        {
            var feedback = Get(id);
            if (feedback == null)
            {
                return null;
            }
            jsonPatch.ApplyTo(feedback);//result gets the values from the patch request
            return feedback;
        }

        public Feedback Remove(Feedback feedback)
        {
            if (feedback == null)
            {
                return null;
            }
            _repo.Remove(feedback);
            return feedback;
        }
    }
}
