﻿using System;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Validators.Generics;

namespace Sales.Services.Validators
{
    public class CityValidator : IValidator<City>
    {
        private IRepository<City> _repo;

        public CityValidator(IRepository<City> repo)
        {
            _repo = repo;
        }

        public bool CanAdd(City obj)
        {
            return _repo.FindOne(city =>
                city.CityName.Equals(obj.CityName, StringComparison.InvariantCultureIgnoreCase)
                ) == null;
        }

        public bool CanEdit(City obj)
        {
            return false;
        }
    }

}
