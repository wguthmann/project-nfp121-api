﻿namespace Sales.Services.Validators.Generics
{
    public interface IValidator<T>
    {
        bool CanAdd(T obj);
        bool CanEdit(T obj);
    }
}