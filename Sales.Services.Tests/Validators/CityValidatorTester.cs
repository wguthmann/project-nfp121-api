using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;
using Sales.Services.Services.ModelsService;

namespace Sales.Services.Tests.Validators
{
    [TestClass]
    public class CityValidatorTester
    {
        private CityValidator _service;
        private Mock<IRepository<City>> _repoMock;

        public CityValidatorTester()
        {
            _repoMock = new Mock<IRepository<City>>();
            _service = new CityValidator(_repoMock.Object);
        }

        [TestMethod]
        public void CanAdd_WithAnyCitiesInDb_ThenTrue()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<City, bool>>>()
            )).Returns((City)null);


            Assert.IsTrue(_service.CanAdd(new City()));
        }

        [TestMethod]
        public void CanAdd_WithExistingCityInDb_ThenFalse()
        {
            _repoMock.Setup(x => x.FindOne(
                It.IsAny<Expression<Func<City, bool>>>()
            )).Returns(new City());

            Assert.IsFalse(_service.CanAdd(new City()));
        }

    }
}
