﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;
using System.Collections.Generic;

namespace SecondLife.Controllers
{

    [ApiController, Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private IService<Product> _service;

        // ProductController
        public ProductController(IService<Product> service)
        {
            _service = service;
        }


        #region CRUD Requests

        [HttpPost]
        public ActionResult<Product> Create(Product product)
        {
            Product res = _service.Add(product);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<Product>> List()
        {
            List<Product> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<Product> JsonPatch(int id,
            [FromBody] JsonPatchDocument<Product> jsonPatch)
        {
            Product product = _service.Patch(id, jsonPatch);
            if (product == null)
                return BadRequest();
            return Ok(product);
        }

        [HttpDelete("{id}")]
        public ActionResult<Product> Delete(int id)
        {
            Product product = _service.Get(id);
            var res = _service.Remove(product);
            if (product == null)
                return BadRequest("Error removing object");
            return Ok(product);
        }

        #endregion CRUD Requests


        #region Custom Requests



        #endregion Custom Requests
    }

}
