﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class ProductTypeController : ControllerBase
    {
        private IProductTypeService _service;

        public ProductTypeController(IProductTypeService service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<ProductType> Create(ProductType productType)
        {
            ProductType res = _service.Add(productType);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<ProductType>> List()
        {
            List<ProductType> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<ProductType> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<ProductType> JsonPatch(int id,
            [FromBody] JsonPatchDocument<ProductType> jsonPatch)
        {
            ProductType productType = _service.Patch(id, jsonPatch);
            if (productType == null)
                return BadRequest();
            return Ok(productType);
        }

        [HttpDelete("{id}")]
        public ActionResult<ProductType> Delete(int id)
        {
            ProductType productType = _service.Get(id);
            var res = _service.Remove(productType);
            if (productType == null)
                return BadRequest("Error removing object");
            return Ok(productType);
        }


        #endregion CRUD Requests


        #region Custom Requests


        #endregion Custom Requests
    }
}
