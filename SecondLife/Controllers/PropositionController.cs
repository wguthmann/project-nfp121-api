﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class PropositionController : ControllerBase
    {
        private IPropositionService _service;

        //PropositionController
        public PropositionController(IPropositionService service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<Proposition> Create(Proposition proposition)
        {
            Proposition res = _service.Add(proposition);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<Proposition>> List()
        {
            List<Proposition> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<Proposition> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<Proposition> JsonPatch(int id,
            [FromBody] JsonPatchDocument<Proposition> jsonPatch)
        {
            Proposition proposition = _service.Patch(id, jsonPatch);
            if (proposition == null)
                return BadRequest();
            return Ok(proposition);
        }

        [HttpDelete("{id}")]
        public ActionResult<Proposition> Delete(int id)
        {
            Proposition proposition = _service.Get(id);
            var res = _service.Remove(proposition);
            if (proposition == null)
                return BadRequest("Error removing object");
            return Ok(proposition);
        }

        #endregion CRUD Requests



        #region Custom Requests



        #endregion Custom Requests
    }
}
