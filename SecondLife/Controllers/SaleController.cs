﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private IService<Sale> _service;

        public SaleController(IService<Sale> service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<Sale> Create(Sale sale)
        {
            Sale res = _service.Add(sale);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<Sale>> List()
        {
            List<Sale> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<Sale> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<Sale> JsonPatch(int id,
            [FromBody] JsonPatchDocument<Sale> jsonPatch)
        {
            Sale sale = _service.Patch(id, jsonPatch);
            if (sale == null)
                return BadRequest();
            return Ok(sale);
        }

        [HttpDelete("{id}")]
        public ActionResult<Sale> Delete(int id)
        {
            Sale sale = _service.Get(id);
            var res = _service.Remove(sale);
            if (sale == null)
                return BadRequest("Error removing object");
            return Ok(sale);
        }


        #endregion CRUD Requests


        #region Custom Requests



        #endregion Custom Requests
    }

}
