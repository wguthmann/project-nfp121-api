﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private IUserService _service;

        // UserController
        public UserController(IUserService service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<User> Create(User user)
        {
            User res = _service.Add(user);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<User>> List()
        {
            List<User> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<User> JsonPatch(int id,
            [FromBody] JsonPatchDocument<User> jsonPatch)
        {
            User user = _service.Patch(id, jsonPatch);
            if (user == null)
                return BadRequest();
            return Ok(user);
        }

        [HttpDelete("{id}")]
        public ActionResult<User> Delete(int id)
        {
            User user = _service.Get(id);
            var res = _service.Remove(user);
            if (user == null)
                return BadRequest("Error removing object");
            return Ok(user);
        }

        #endregion CRUD Requests


        #region Custom Requests



        #endregion Custom Requests
    }
}
