﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class CityController : ControllerBase
    {

        // GET: CityController
        private IService<City> _service;

        public CityController(IService<City> service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<City> Create(City city)
        {
            City res = _service.Add(city);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<City>> List()
        {
            List<City> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<City> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<City> JsonPatch(int id,
            [FromBody] JsonPatchDocument<City> jsonPatch)
        {
            City city = _service.Patch(id, jsonPatch);
            if (city == null)
                return BadRequest();
            return Ok(city);
        }

        [HttpDelete("{id}")]
        public ActionResult<City> Delete(int id)
        {
            City city = _service.Get(id);
            var res = _service.Remove(city);
            if (city == null)
                return BadRequest("Error removing object");
            return Ok(city);
        }

        #endregion CRUD Requests

        #region Custom Requests



        #endregion Custom Requests

    }
}
