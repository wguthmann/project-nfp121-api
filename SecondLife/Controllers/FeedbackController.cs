﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sales.Model.Entities;
using Sales.Services.Services;

namespace SecondLife.Controllers
{
    [ApiController, Route("api/[controller]")]
    public class FeedbackController : ControllerBase
    {
        // GET: FeedbackController
        private IFeedbackService _service;

        public FeedbackController(IFeedbackService service)
        {
            _service = service;
        }

        #region CRUD Requests

        [HttpPost]
        public ActionResult<Feedback> Create(Feedback feedback)
        {
            Feedback res = _service.Add(feedback);
            if (res == null)
            {
                return BadRequest("invalid request");
            }
            return Ok(res);
        }

        [HttpGet]
        public ActionResult<List<Feedback>> List()
        {
            List<Feedback> res = _service.List();
            if (res.Count == 0)
            {
                return NoContent();
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<Feedback> Get(int id)
        {
            return _service.Get(id);

        }

        [HttpPatch("{id}")]
        public ActionResult<Feedback> JsonPatch(int id,
            [FromBody] JsonPatchDocument<Feedback> jsonPatch)
        {
            Feedback feedback = _service.Patch(id, jsonPatch);
            if (feedback == null)
                return BadRequest();
            return Ok(feedback);
        }

        [HttpDelete("{id}")]
        public ActionResult<Feedback> Delete(int id)
        {
            Feedback feedback = _service.Get(id);
            var res = _service.Remove(feedback);
            if (feedback == null)
                return BadRequest("Error removing object");
            return Ok(feedback);
        }

        #endregion CRUD Requests



    }
}
