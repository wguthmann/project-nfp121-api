﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class PropositionRepository : GenericRepository<Proposition>, IPropositionRepository
    {
        private readonly SaleDbContext _context;

        public PropositionRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Proposition> All()
        {
            return _context.Propositions.ToList();
        }

        public Proposition One(int id)
        {
            return _context.Propositions.Find(id);
        }

        public Proposition Add(Proposition proposition)
        {
            _context.Add(proposition);
            _context.SaveChanges();
            return proposition;
        }

        public Proposition Update(Proposition proposition)
        {
            _context.Attach(proposition);
            _context.SaveChanges();
            return proposition;
        }

        public Proposition Remove(Proposition proposition)
        {
            _context.Remove(proposition);
            _context.SaveChanges();
            return proposition;
        }

        public bool Exists(Proposition proposition)
        {
            var dbSales = _context.Propositions.FirstOrDefault(x => x.Id == proposition.Id);
            return dbSales != null;
        }
    }
}
