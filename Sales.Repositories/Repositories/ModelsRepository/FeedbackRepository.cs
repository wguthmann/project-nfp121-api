﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Model.Interface;

namespace Sales.Repositories.Repositories.ModelsRepository
{

    public class FeedbackRepository : GenericRepository<Feedback>, IFeedbackRepository
    {
        private readonly SaleDbContext _context;

        public FeedbackRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Feedback> All()
        {
            return _context.Feedbacks.ToList();
        }

        public Feedback One(int id)
        {
            return _context.Feedbacks.Find(id);
        }

        public Feedback Add(Feedback feedback)
        {
            _context.Add(feedback);
            _context.SaveChanges();
            return feedback;
        }

        public Feedback Update(Feedback feedback)
        {
            _context.Attach(feedback);
            _context.SaveChanges();
            return feedback;
        }

        public Feedback Remove(Feedback feedback)
        {
            _context.Remove(feedback);
            _context.SaveChanges();
            return feedback;
        }

        public bool Exists(Feedback feedback)
        {
            var dbSales = _context.Feedbacks.FirstOrDefault(x => x.Id == feedback.Id);
            return dbSales != null;
        }
    }
}