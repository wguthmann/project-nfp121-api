﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Repositories.Repositories;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly SaleDbContext _context;

        public UserRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }
        public List<User> All()
        {
            return _context.Users.ToList();
        }

        public User One(int id)
        {
            return _context.Users.Find(id);
        }

        public User Add(User user)
        {
            _context.Add(user);
            _context.SaveChanges();
            return user;
        }

        public User Update(User user)
        {
            _context.Attach(user);
            _context.SaveChanges();
            return user;
        }

        public User Remove(User user)
        {
            _context.Remove(user);
            _context.SaveChanges();
            return user;
        }

        public bool Exists(User user)
        {
            var dbSales = _context.Users.FirstOrDefault(x => x.Id == user.Id);
            return dbSales != null;
        }
    }
}
