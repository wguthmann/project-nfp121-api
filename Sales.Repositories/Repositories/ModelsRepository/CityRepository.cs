﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Sales.Model;
using Sales.Model.Entities;
using Sales.Model.Interface;

namespace Sales.Repositories.Repositories.ModelsRepository
{
   
    public class CityRepository : GenericRepository<City>, ICityRepository
    {
        private readonly SaleDbContext _context;

        public CityRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<City> All()
        {
            return _context.Cities.ToList();
        }

        public City One(int id)
        {
            return _context.Cities.Find(id);
        }

        public City Add(City city)
        {
            _context.Add(city);
            _context.SaveChanges();
            return city;
        }

        public City Update(City city)
        {
            _context.Attach(city);
            _context.SaveChanges();
            return city;
        }

        public City Remove(City city)
        {
            _context.Remove(city);
            _context.SaveChanges();
            return city;
        }

        public bool Exists(City city)
        {
            var dbSales = _context.Cities.FirstOrDefault(x => x.Id == city.Id);
            return dbSales != null;
        }
    }
}
