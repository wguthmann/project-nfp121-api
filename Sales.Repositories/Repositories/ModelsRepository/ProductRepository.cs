﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly SaleDbContext _context;

        public ProductRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Product> All()
        {
            return _context.Products.ToList();
        }

        public Product One(int id)
        {
            return _context.Products.Find(id);
        }

        public Product Add(Product product)
        {
            _context.Add(product);
            _context.SaveChanges();
            return product;
        }

        public Product Update(Product product)
        {
            _context.Attach(product);
            _context.SaveChanges();
            return product;
        }

        public Product Remove(Product product)
        {
            _context.Remove(product);
            _context.SaveChanges();
            return product;
        }

        public bool Exists(Product product)
        {
            var dbSales = _context.Products.FirstOrDefault(x => x.Id == product.Id);
            return dbSales != null;
        }
    }
}
