﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sales.Model.Entities;
using Sales.Model;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class SaleRepository : GenericRepository<Sale>, ISaleRepository
    {
    private readonly SaleDbContext _context;

    public SaleRepository(SaleDbContext context) : base(context)
    {
        _context = context;
    }

    public List<Sale> All()
    {
        return _context.Sales.ToList();
    }

    public Sale One(int id)
    {
        return _context.Sales.Find(id);
    }

    public Sale Add(Sale sale)
    {
        _context.Add(sale);
        _context.SaveChanges();
        return sale;
    }

    public Sale Update(Sale sale)
    {
        _context.Attach(sale);
        _context.SaveChanges();
        return sale;
    }

    public Sale Remove(Sale sale)
    {
        _context.Remove(sale);
        _context.SaveChanges();
        return sale;
    }

    public bool Exists(Sale sale)
    {
        var dbSales = _context.Sales.FirstOrDefault(x => x.Id == sale.Id);
        return dbSales != null;
    }
    }
}
