﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales.Model;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories.ModelsRepository
{
    public class ProductTypeRepository : GenericRepository<ProductType>, IProductTypeRepository
    {

        private readonly SaleDbContext _context;

        public ProductTypeRepository(SaleDbContext context) : base(context)
        {
            _context = context;
        }

        public List<ProductType> All()
        {
            return _context.ProductTypes.ToList();
        }

        public ProductType One(int id)
        {
            return _context.ProductTypes.Find(id);
        }

        public ProductType Add(ProductType productType)
        {
            _context.Add(productType);
            _context.SaveChanges();
            return productType;
        }

        public ProductType Update(ProductType productType)
        {
            _context.Attach(productType);
            _context.SaveChanges();
            return productType;
        }

        public ProductType Remove(ProductType productType)
        {
            _context.Remove(productType);
            _context.SaveChanges();
            return productType;
        }

        public bool Exists(ProductType productType)
        {
            var dbSales = _context.ProductTypes.FirstOrDefault(x => x.Id == productType.Id);
            return dbSales != null;
        }
    }
}
