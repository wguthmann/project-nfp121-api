﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Sales.Model.Entities;

namespace Sales.Repositories.Repositories
{
    public interface ISaleRepository : IRepository<Sale>
    {
    }

    public interface IUserRepository : IRepository<User>
    {
    }

    public interface IPropositionRepository : IRepository<Proposition>
    {
    }

    public interface IProductTypeRepository : IRepository<ProductType>
    {
    }

    public interface IProductRepository : IRepository<Product>
    {
    }

    public interface IFeedbackRepository : IRepository<Feedback>
    {
    }

    public interface ICityRepository : IRepository<City>
    {
    }

    public interface IRepository<T> where T : class
    {
        List<T> All();
        T One(int id);
        T Add(T sale);
        T Update(T sale);
        T Remove(T sale);
        bool Exists(T sale);
        T FindOne(Expression<Func<T, bool>> conditon);
    }
}
