﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Sales.Model;
using Sales.Model.Interface;

namespace Sales.Repositories.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class, IIdentityObject
    {
        private readonly SaleDbContext _context;

        public GenericRepository(SaleDbContext context)
        {
            _context = context;
        }

        public List<T> All()
        {
            return _context.Set<T>().ToList();
        }

        public T One(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public T Add(T obj)
        {
            _context.Add(obj);
            _context.SaveChanges();
            return obj;
        }

        public T Update(T obj)
        {
            _context.Attach(obj);
            _context.SaveChanges();
            return obj;
        }

        public T Remove(T obj)
        {
            _context.Remove(obj);
            _context.SaveChanges();
            return obj;
        }

        public bool Exists(T obj)
        {
            var res = _context.Set<T>().FirstOrDefault(x => x.Id == obj.Id);
            return res != null;
        }

        public T FindOne(Expression<Func<T, bool>> conditon)
        {
            return _context.Set<T>().FirstOrDefault(conditon);
        }
    }
}
